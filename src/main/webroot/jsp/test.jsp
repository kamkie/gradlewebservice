<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" content="text/html">
    <title>Hello word!</title>
</head>
<body>
<h1>Hello word!</h1>

<div>
    <pre>
        <h2>Hi ${it.name} (${it.id})</h2>
        <p>your role is ${it.role}</p>
        <p>${obj}</p>
    </pre>
</div>

</body>
</html>