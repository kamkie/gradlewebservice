package com.example.dao;

import com.example.model.User;

public class FakeUsersRepository implements IUsersRepository {

    public User getUserForCredentials(String userName, String password) {
        User user = null;

        if ("kamkie".equals(userName) && "password".equals(password)) {
            user = new User();
            user.setName(userName);
            user.setPassword(password);
            user.setRole("user");
        }

        return user;
    }

    @Override
    public User getUserByName(String userName) {
        return null;
    }

    @Override
    public User getUserById(Integer id) {
        return null;
    }
}
