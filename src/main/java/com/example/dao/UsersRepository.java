package com.example.dao;

import com.example.model.User;
import com.example.security.IPasswordProvider;
import org.hibernate.CacheMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class UsersRepository implements IUsersRepository {

    private final Logger logger = LoggerFactory.getLogger(UsersRepository.class);
    private final SessionFactory sessionFactory = LocalSessionFactoryBuilder.sessionFactory;

    @Inject
    IPasswordProvider passwordProvider;

    private Session getSession() {
        logger.debug("get session");
        Session session = sessionFactory.openSession();
        session.clear();
        session.setCacheMode(CacheMode.IGNORE);

        return session;
    }

    private void sessionClose(Session session, Object user) {
        if (user != null) {
            session.evict(user);
        }
        session.close();
    }

    @Override
    public User getUserForCredentials(String userName, String password) {
        if (userName == null || password == null) {
            return null;
        }

        logger.debug("get user {} with password {}", userName, password);
        Session session = getSession();
        Object user = session.createCriteria(User.class).setCacheable(false).add(Restrictions.eq("name", userName)).uniqueResult();
        sessionClose(session, user);
        if (user instanceof User) {
            User user1 = (User) user;
            if (passwordProvider.checkPassword(user1.getPassword(), password)) {
                return user1;
            } else {
                logger.warn("password not mach");
            }
        }
        return null;
    }

    @Override
    public User getUserByName(String userName) {
        Session session = getSession();
        Object user = session.createCriteria(User.class).setCacheable(false).add(Restrictions.eq("name", userName)).uniqueResult();
        sessionClose(session, user);
        if (user instanceof User) {
            return (User) user;
        } else {
            return null;
        }
    }

    @Override
    public User getUserById(Integer id) {
        Session session = getSession();
        Object user = session.get(User.class, id);
        sessionClose(session, user);
        if (user instanceof User) {
            return (User) user;
        } else {
            return null;
        }
    }

}
