package com.example.dao;

import com.example.model.User;

public interface IUsersRepository {

    public User getUserForCredentials(String userName, String password);

    public User getUserByName(String userName);

    public User getUserById(Integer id);
}
