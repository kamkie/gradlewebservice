package com.example.rest;

import com.example.model.User;
import org.glassfish.jersey.server.mvc.Viewable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.ManagedBean;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

@ManagedBean
@Path("/hello")
public class HelloWorld {

    private final Logger logger = LoggerFactory.getLogger(HelloWorld.class);

    @Context
    SecurityContext securityContext;

    @Path("{name}")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String helloName(@PathParam("name") String name) {
        logger.info("hello name");
        return "Hello " + name + "!!!!";
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        logger.info("hello you");
        return "Hello you!!!!";
    }

    @GET
    @Path("html")
    @Produces("text/html")
    public Viewable get(@Context HttpServletRequest request) {
        logger.info("test jsp");
        request.setAttribute("obj", "Super");
        return new Viewable("/test.jsp", securityContext.getUserPrincipal());
    }
}