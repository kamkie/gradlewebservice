package com.example.security;

public interface IPasswordProvider {

    public String getStaticSalt();

    public String getRandomSalt();
    
    public String getPasswordHash(String password);

    public boolean checkPassword(String hash, String password);
}
