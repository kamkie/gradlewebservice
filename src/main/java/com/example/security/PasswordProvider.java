package com.example.security;

import org.apache.commons.codec.digest.DigestUtils;

import java.util.Base64;
import java.util.Random;

public class PasswordProvider implements IPasswordProvider {

    private final int saltLength = 16;
    private final String separator = "::";
    private final String staticSalt = "1234567890123456";

    @Override
    public String getStaticSalt() {
        return staticSalt;
    }

    @Override
    public String getRandomSalt() {
        byte[] bytes = new byte[saltLength];
        Random random = new Random();
        random.nextBytes(bytes);

        return Base64.getEncoder().encodeToString(bytes).substring(0, saltLength);
    }

    @Override
    public String getPasswordHash(String password) {
        String salt = getRandomSalt();

        return computeHash(password, salt);
    }

    @Override
    public boolean checkPassword(String hash, String password) {
        String[] parts = hash.split("::");
        if (hash != null && password != null && parts.length == 2) {
            String salt = parts[1];
            return hash.equals(computeHash(password, salt));
        }
        return false;
    }

    private String computeHash(String password, String salt) {
        return DigestUtils.sha512Hex(getStaticSalt() + password + salt) + separator + salt;
    }
}
