package com.example.security;

import com.example.dao.IUsersRepository;
import com.example.model.User;

import javax.annotation.ManagedBean;
import javax.inject.Inject;
import javax.ws.rs.core.SecurityContext;
import java.security.Principal;

@ManagedBean
public class BasicSecurityContext implements SecurityContext {

    protected User user;
    protected boolean secure;

    @Inject
    IUsersRepository usersRepository;

    public BasicSecurityContext() {
    }

    public void setUser(String userName, String password) {
        this.user = usersRepository.getUserForCredentials(userName, password);
    }

    public void setSecure(boolean secure) {
        this.secure = secure;
    }

    @Override
    public Principal getUserPrincipal() {
        return user;
    }

    public User getUser() {
        return user;
    }

    @Override
    public boolean isUserInRole(String role) {
        return user.getRole() != null && user.getRole().equalsIgnoreCase(role);
    }

    @Override
    public boolean isSecure() {
        return secure;
    }

    @Override
    public String getAuthenticationScheme() {
        return BASIC_AUTH;
    }
}
