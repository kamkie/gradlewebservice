package com.example.app;

import javax.ws.rs.core.Feature;
import javax.ws.rs.core.FeatureContext;

public class DiFeature implements Feature {

    @Override
    public boolean configure(FeatureContext featureContext) {
        featureContext.register(new DiBinder());
        return true;
    }
}