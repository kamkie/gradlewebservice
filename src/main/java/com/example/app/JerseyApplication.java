package com.example.app;

import com.example.filter.AuthFilter;
import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.message.GZipEncoder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.EncodingFilter;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.glassfish.jersey.server.mvc.jsp.JspMvcFeature;
import org.glassfish.jersey.servlet.ServletProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("/*")
public class JerseyApplication extends ResourceConfig {

    private final Logger logger = LoggerFactory.getLogger(JerseyApplication.class);

    public JerseyApplication() {
        SLF4JBridgeHandler.removeHandlersForRootLogger();
        SLF4JBridgeHandler.install();

        logger.info("application start -----------------------------------------------------------------------------");

        property(JspMvcFeature.TEMPLATES_BASE_PATH, "/jsp");
        property(ServletProperties.FILTER_STATIC_CONTENT_REGEX, "(/(image|js|css)/?.*)|(/.*\\.jsp)|(/WEB-INF/.*\\.jsp)|"
                + "(/WEB-INF/.*\\.jspf)|(/.*\\.html)|(/favicon\\.ico)|(/robots\\.txt)");
        register(DiFeature.class);
        register(LoggingFilter.class);
        register(EncodingFilter.class);
        register(GZipEncoder.class);
        register(JacksonFeature.class);
        register(JspMvcFeature.class);
        register(RolesAllowedDynamicFeature.class);
        register(AuthFilter.class);
        packages("com.example.app");
        packages("com.example.dao");
        packages("com.example.filter");
        packages("com.example.rest");
        packages("com.example.security");
        packages("com.example.server");
    }

}
