package com.example.app;

import com.example.dao.IUsersRepository;
import com.example.dao.UsersRepository;
import com.example.security.BasicSecurityContext;
import com.example.security.IPasswordProvider;
import com.example.security.PasswordProvider;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

public class DiBinder extends AbstractBinder {

    @Override
    protected void configure() {
        bind(UsersRepository.class).to(IUsersRepository.class);
        bind(BasicSecurityContext.class).to(BasicSecurityContext.class);
        bind(PasswordProvider.class).to(IPasswordProvider.class);
    }

}