package com.example.filter;

import com.example.security.BasicSecurityContext;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.Base64;

@Provider
@PreMatching
@Priority(Priorities.AUTHORIZATION)
public class AuthFilter implements ContainerRequestFilter {

    @Inject
    BasicSecurityContext securityContext;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        requestContext.setSecurityContext(securityContext);
        String authHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
        if (authHeader != null && authHeader.contains("Basic")) {
            String authBase64 = authHeader.substring(authHeader.indexOf("Basic ") + "Basic ".length());
            String auth = new String(Base64.getDecoder().decode(authBase64));
            if (auth != null && auth.contains(":")) {
                String[] userAndPass = auth.split(":");
                if (userAndPass.length == 2) {
                    securityContext.setUser(userAndPass[0], userAndPass[1]);
                    securityContext.setSecure("https".equals(requestContext.getUriInfo().getRequestUri().getScheme()));
                }
                if (securityContext.getUserPrincipal() != null) {
                    return;
                }
            }
        }

        requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).header(HttpHeaders.WWW_AUTHENTICATE, "Basic realm=\"Restricted\"").build());
    }
}
