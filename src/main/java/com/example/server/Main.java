package com.example.server;

import com.example.app.JerseyApplication;
import org.apache.jasper.servlet.JspServlet;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.servlet.FilterRegistration;
import org.glassfish.grizzly.servlet.ServletRegistration;
import org.glassfish.grizzly.servlet.WebappContext;
import org.glassfish.jersey.servlet.ServletContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.DispatcherType;
import java.io.IOException;
import java.util.EnumSet;

public class Main {

    static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static final String HOST = "localhost";
    public static final int PORT = 8080;
    private static final String JSP_CLASSPATH_ATTRIBUTE = "org.apache.catalina.jsp_classpath";

    public static void main(String[] args) {
        logger.info("grizzly starting");

        WebappContext webappContext = new WebappContext("grizzly web context", "/");

        ServletContainer servletContainer = new ServletContainer(new JerseyApplication());

        FilterRegistration registration = webappContext.addFilter("Jersey", servletContainer);
        registration.setAsyncSupported(true);
        registration.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), "/*");

        ServletRegistration jspRegistration = webappContext.addServlet("JSPContainer", JspServlet.class.getName());
        jspRegistration.setLoadOnStartup(1);
        jspRegistration.addMapping("/*");

        webappContext.setAttribute(JSP_CLASSPATH_ATTRIBUTE, System.getProperty("java.class.path"));

        HttpServer server = HttpServer.createSimpleServer("/", HOST, PORT);

        webappContext.deploy(server);
        try {
            server.start();
        } catch (IOException e) {
            logger.error("grizzly cannot start", e);
        }

        try {
            Thread.currentThread().join();
        } catch (InterruptedException e) {
            logger.error("grizzly cannot join", e);
        }

    }

}
